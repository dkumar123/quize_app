
import React, { useState, useEffect } from 'react';
import axios from 'axios';
//import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'
//import Email from './Email';
import './App.css';
import { Row, Col, Container, Form } from 'react-bootstrap';
import img1 from './img1.svg';
import img2 from './img2.svg';
import img3 from './img3.svg';
import img4 from './img4.svg';
import img5 from './img5.svg';
import img6 from './img6.svg';
import img7 from './img7.svg';
export default function Header() {


    const [codes, setCodes] = useState([])
    useEffect(() => {
        axios
            .get("http://instapreps.com/api/country_code")
            .then((res) => {
                console.log(res.data.data);
                let tempArr = [];
                for (let i = 0; i < 25; i++) {
                    tempArr.push({
                        id: res.data.data[i].id,
                        code: res.data.data[i].code
                    });
                }
                setCodes(tempArr);
            })
            .catch((err) => console.log(err));
    }, []);

    const [articles, setArticles] = useState([])
    useEffect(() => {
        axios
            .get("https://apistaging.7classes.com/api/v1/boards")
            .then((res) => {
                console.log(res.data.data);
                let tempArr = [];
                for (let i = 0; i < 9; i++) {
                    tempArr.push({
                        id: res.data.data[i].id,
                        name: res.data.data[i].name
                    });
                }
                setArticles(tempArr);
            })
            .catch((err) => console.log(err));
    }, []);

    const [classes, setClasses] = useState([])
    useEffect(() => {
        axios
            .get("https://apistaging.7classes.com/api/v1/grades")
            .then((res) => {
                console.log(res.data.data);
                let tempArr = [];
                for (let i = 0; i < 9; i++) {
                    tempArr.push({
                        id: res.data.data[i].id,
                        name: res.data.data[i].name
                    });
                }
                setClasses(tempArr);
            })
            .catch((err) => console.log(err));
    }, []);
    const [subject, setSubject] = useState([])
    useEffect(() => {
        axios
            .get("https://apistaging.7classes.com/api/v1/subjects")
            .then((res) => {
                console.log(res.data.data);
                let tempArr = [];
                for (let i = 0; i < 5; i++) {
                    tempArr.push({
                        id: res.data.data[i].id,
                        name: res.data.data[i].name
                    });
                }
                setSubject(tempArr);
            })
            .catch((err) => console.log(err));
    }, []);
    const [name, setName] = useState([]);
    const [email, setEmail] = useState([]);
    const [phone, setPhone] = useState([]);
    const [board_id, setBoard_id] = useState([])
    const [grade_id, setGrade_id] = useState([]);
    const [pcode, setPcode] = useState([]);
    const [subjects, setSubjects] = useState([]);
    const [pname, setPname] = useState([]);
    const [click, setClick] = useState('yes');

    
         
       function Resister(){
           console.warn(pcode,phone,name,email,)
       const params = JSON.stringify({
            name,
            email,
            phone,
            board_id,
            grade_id
            
            
            })
            
            axios.post('https://apistaging.7classes.com/api/v1/register', params,{

                "headers": {
                
                "content-type": "application/json",
                
                },
                
                })
                .then(function(response) {

                    console.log(response.data);
                    
                    })
                    
                    .catch(function(error) {
                    
                    console.log(error.response.data);
                    
                    });
                    
                
                
    }

    return (
        <div className='container'>

            <Container>
                <Row>
                    <Col className='box'>
                        <div className=''>
                            <p class="text-left"><h1>Why choose 7 class?</h1></p>
                        </div>
                        <div class="d-grid gap-3">
                            <div class="p-2 bg-white shadow-primary rounded-xl border">
                                <div>
                                    <Row>
                                        <Col sm={2}>
                                            <img src={img1} alt='img'></img>
                                        </Col >
                                        <Col sm={10}><dd><b>Only 7 student in one batch</b> with</dd>teacher from IITs,NITs & super30
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <div class="p-2 bg-white shadow-primary rounded-xl border">
                                <div>
                                    <Row>
                                        <Col sm={2}>
                                            <img src={img2} alt='img'></img>
                                        </Col >
                                        <Col sm={10}><dd><b>Student-Cetric unique DPP</b>(Daily Practice Problem), </dd>with ONLINE Dout Clearing facilities
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <div class="p-2 bg-white shadow-primary rounded-xl border">
                                <div>
                                    <Row>
                                        <Col sm={2}>
                                            <img src={img3} alt='img'></img>
                                        </Col >
                                        <Col sm={10}><dd><b>Competition mixed with Personal Attention</b> and</dd>Self Study Routine for each student
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <div class="p-2 bg-white shadow-primary rounded-xl border">
                                <div>
                                    <Row>
                                        <Col sm={2}>
                                            <img src={img4} alt='img'></img>
                                        </Col >
                                        <Col sm={10}><dd><b>Tips and Tricks for learning</b>techniques, </dd>Stress & Time Management
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <div class="p-2 bg-white shadow-primary rounded-xl border">
                                <div>
                                    <Row>
                                        <Col sm={2}>
                                            <img src={img5} alt='img'></img>
                                        </Col >
                                        <Col sm={10}><dd><b>Track and Rectify Confidence Issues with our </b> Learning</dd>Management System
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <div class="p-2 bg-white shadow-primary rounded-xl border">
                                <div>
                                    <Row>
                                        <Col sm={2}>
                                            <img src={img6} alt='img'></img>
                                        </Col >
                                        <Col sm={10}><dd><b>Dedicated Performance Manager</b>for</dd>optimal performance
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <div class="p-2 bg-white shadow-primary rounded-xl border">
                                <div>
                                    <Row>
                                        <Col sm={2}>
                                            <img src={img7} alt='img'></img>
                                        </Col >
                                        <Col sm={10}><dd><b>Dedicated Counselling & Weekly</b> Motivation</dd>session for Student and Parent
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                        </div>

                    </Col>


                    <Col className='box'>
                        <div className='p-2 bg-white shadow-primary rounded-xl border'>
                            <div classeName=''>
                                <h2>Register </h2>
                                <p>19 slots left!</p>
                            </div>
                            <Form accept-charset="UTF-8" action="/clients" method="post">
                                <Row className="mb-3">
                                    <Col sm={4}>
                                        <Form.Group as={Col} controlId="formGridState" >
                                            <Form.Label></Form.Label>
                                            <Form.Select defaultValue="choose.." value={pcode} onChange={(e) => { setPcode(e.target.value) }}name='pcode' >
                                                {codes.map(items => {
                                                    return (
                                                        <option key={items.id} value={items.code}>{items.code}</option>
                                                    )
                                                })}
                                            </Form.Select>
                                        </Form.Group>
                                    </Col>

                                    <Form.Group as={Col} controlId="formGridPassword">
                                        <Form.Label></Form.Label>
                                        <Form.Control type="mobile no" placeholder="Mobile No" value={phone} onChange={(e) => { setPhone(e.target.value) }} name='mobile' />
                                    </Form.Group>
                                    <Form.Group controlId="formGridPassword">
                                        <Form.Label></Form.Label>
                                        <Form.Control type=" " placeholder="kids Name" value={name} onChange={(e) => { setName(e.target.value) }} name='name' />
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="formGridState">
                                        <Form.Label></Form.Label>
                                        <Form.Select defaultValue="Choose..." value={board_id} onChange={(e) => { setBoard_id (e.target.value) }} name='boards'>
                                            {articles.map(items => {
                                                return (

                                                    <option key={items.id} value={items.id} >{items.name}</option>
                                                )
                                            })}
                                        </Form.Select>




                                    </Form.Group>

                                    <Form.Group as={Col} controlId="formGridState">
                                        <Form.Label></Form.Label>
                                        <Form.Select defaultValue="" value={grade_id} onChange={(e) => { setGrade_id(e.target.value) }} name='standard' >
                                            {classes.map(items => {
                                                return (
                                                    <option key={items.id} value={items.id} >{items.name}</option>
                                                )
                                            })}
                                        </Form.Select>
                                    </Form.Group>
                                    <Form.Group controlId="formGridState">
                                        <Form.Label></Form.Label>
                                        <Form.Select defaultValue="" value={subjects} onChange={(e) => { setSubjects(e.target.value) }} name='subjects'>
                                            {subject.map(items => {
                                                return (
                                                    <option key={items.id} value={items.name} >{items.name}</option>
                                                )
                                            })}


                                        </Form.Select>
                                    </Form.Group>
                                    <Form.Group controlId="formGridEmail">
                                        <Form.Label></Form.Label>
                                        <Form.Control type="mobile no" placeholder="Parent's Email id" value={email} onChange={(e) => { setEmail(e.target.value) }} name='email' />
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="formGridState">
                                        <Form.Label></Form.Label>
                                        <Form.Control type="mobile no" placeholder="Parents Name" value={pname} onChange={(e) => { setPname(e.target.value) }} name='pname' />
                                    </Form.Group>
                                </Row>
                                <div>
                                    <Row>
                                        <Col sm={8}>Do you have laptop/pc/smartphone at home for class</Col>
                                        <Col sm={4}><div class="form-check form-check-inline">

                                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" checked={click === 'yes'} value='yes' onClick={() => setClick('yes')}></input>
                                            <label class="form-check-label" for="inlineCheckbox1" >yes</label>
                                        </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked={click === 'no'} value='no' onClick={() => setClick('no')} />
                                                <label class="form-check-label" for="inlineCheckbox2">no </label>
                                            </div>
                                        </Col>

                                    </Row>

                                </div>
                                <div className='sm-4 button_pad'>
                                    <div className='btn_div'>
                                        <button type="button" onClick={Resister} class="button">Register</button>
                                    </div>
                                </div>
                                <div className="">
                                    <p class="text-center">By clicking Sign up you agree to our terms of service
                                    </p>
                                </div>
                                <div className="">
                                 <p class="text-center">Already registered? please Login with <a href="/"> Email/Mobile</a>
                                         </p> 
                                    
                                    </div>
                                <div className="">
                                    <p class="text-center">Already registered as teacher? please Login with <a href="/">
                                        Email/ Mobile</a>.
                                    </p>
                                </div>
                                
                                 </Form>
                        </div>
                    </Col>




                </Row>

            </Container>
        </div>
        

    )
}                                      




